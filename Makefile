all: js css
js:
	gulp js
css:
	gulp scss
clean:
	rm -rf js/dist/
	rm -rf css/dist/
install:
	npm install && composer install
