# MASTifF - Myowncode AweSome Tamaranga Framework 

# NGINX SETUP
- add to nginx config 
```
location ~ ^/(js|css)/(mastiff|mastiffApp)/(.*)$ {
    alias $projDir/$2/$1/dist/$3;
}
```
see mastiff/man/nginx for more information and config templates

# HTTPD SETUP

- add to httpd config
```
AliasMatch ^/(js|css)/(mastiff|mastiffApp)/(.*)$ /path/to/project/$2/$1/dist/$3;
```

# USAGE
Integration: 
- Add MASTifF from git ```git submodule add [path/to/app/.git]```
- Run ```make install && make``` to install node packages and compile js and scss files
- To include js into view use ``` mastiff\tamaranga\Tpl::includeJS(['some_js'])```. Use only file name.
- To include debug js - add to filename ```-debug``` string. For example ```myfile-debug```
- Add to ```bff.php``` to ```init()``` function
    ```        
    require_once PATH_BASE . 'mastiff/autoload.php';
    ```
    And if you want to use ORM
    ```        
    require_once PATH_BASE . 'mastiffApp/db/config/database.php';
    ```

Clone: 
- Add MASTifF from git ```git submodule init && git submodule update```
- Run ```cd mastiff && make install && cd ../mastiffApp && make install``` to install node packages and compile js and scss files
- To include js into view use ``` mastiff\tamaranga\Tpl::includeJS(['some_js'])```. Use only file name.
- To include debug js - add to filename ```-debug``` string. For example ```myfile-debug```
- Run install script
    ```
    php mastiff.php tamaranga:install -U dbuser [-P dbpass] -d db -s hostname.dev
    ```

#CODE USAGE
Static files:
- To include js into view use ``` mastiff\tamaranga\Tpl::includeJS(['some_js'])``` or ```mastiffApp\tamaranga\Tpl::includeJS(['some_js'])```. Use only file name.
- To include debug js - add to filename ```-debug``` string. For example ```myfile-debug```
ORM:
- Mastiff uses Eloquent ORM. See https://laravel.com/docs/master/eloquent
- Models classes stored in /mastiffApp/models/[Model.php]