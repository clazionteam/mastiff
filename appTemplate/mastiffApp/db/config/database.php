<?php

require_once PATH_BASE . 'mastiffApp/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;

$capsule->addConnection([
    'driver' => \config::sys('db.type'),
    'host' => \config::sys('db.host'),
    'port' => \config::sys('db.port'),
    'database' => \config::sys('db.name'),
    'username' => \config::sys('db.user'),
    'password' => \config::sys('db.pass'),
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix' => \config::sys('db.prefix')
]);

$capsule->bootEloquent();