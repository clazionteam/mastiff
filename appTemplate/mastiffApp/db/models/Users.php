<?php

namespace mastiffApp\db\models;
use mastiffApp\db\models\orm\OrmModel;

class Users extends OrmModel
{
    protected $table = 'users';
    public $timestamps = false;
    protected $primaryKey = 'user_id';
}