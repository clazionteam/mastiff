<?php
if (!getenv('PHINX_ENVIRONMENT')) {
    putenv('PHINX_ENVIRONMENT=production');
}
$config = require_once '../config/sys.php';
$dbData = [
    'adapter'       => $config['db.type'],
    'host'          => $config['db.host'],
    'name'          => $config['db.name'],
    'user'          => $config['db.user'],
    'pass'          => $config['db.pass'],
    'port'          => $config['db.port'],
    'table_prefix'  => $config['db.prefix']
];

return [
    'paths'        => [
        'migrations' => 'db/migrations',
        'seeds' => 'db/seeds'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database'        => $config['db.name'],
        'development'             => $dbData,
        'production'              => $dbData,
    ]
];
