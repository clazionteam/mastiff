<?php

namespace mastiffApp\tamaranga;


class Tpl extends \mastiff\tamaranga\Tpl
{
    protected static $dir ='mastiffApp';

    /**
     * @inheritdoc
     */
    public static function includeJS($aJs)
    {
        parent::$dir = self::$dir;
        return parent::includeJS($aJs);
    }

    /**
     * @inheritdoc
     */
    public static function includeCSS($aCss)
    {
        parent::$dir = self::$dir;
        return parent::includeCSS($aCss);
    }
}