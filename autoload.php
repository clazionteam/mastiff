<?php

function autoload ($pClassName) {

    $className = ltrim($pClassName, '\\');
    $className = str_replace('\\', DS, $className);
    if(file_exists(PATH_BASE . $className . ".php")) {
        require(PATH_BASE . $className . ".php");
        return true;
    }
    return false;
}

function _st($key)
{
    $st = \mastiff\myowncode\StaticText\Base::getInstance();
    return $st->getStaticText($key);
}

require_once 'vendor/autoload.php';

if(file_exists('../mastiffApp/vendor/autoload.php'))
    require_once '../mastiffApp/vendor/autoload.php';

spl_autoload_register("autoload");
