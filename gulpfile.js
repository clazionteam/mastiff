'use strict';

const gulp = require("gulp");
const sourcemaps = require("gulp-sourcemaps");
const babel = require("gulp-babel");
const concat = require("gulp-concat");
const minify = require('gulp-minify');
const cleanCSS = require('gulp-clean-css');

const sass = require('gulp-sass');


gulp.task("js", function () {
    return gulp.src("js/src/**/*.js")
        .pipe(sourcemaps.init())
        .pipe(babel({'presets':['es2015']}))
        .pipe(sourcemaps.write("."))
        .pipe(minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest("js/dist"));
});


gulp.task('scss', function () {
    return gulp.src('css/src/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest('css/dist'));
});

gulp.task('scss:watch', function () {
    gulp.watch('css/src/**/*.scss', ['scss']);
});

gulp.task('js:watch', function () {
    gulp.watch('js/src/**/*.js', ['js']);
});