'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AdminItemBlock = function () {
    _createClass(AdminItemBlock, [{
        key: 'url',
        get: function get() {
            return this._url;
        },
        set: function set(value) {
            this._url = value;
        }
    }, {
        key: 'itemID',
        get: function get() {
            return this._itemID;
        },
        set: function set(value) {
            this._itemID = value;
        }
    }, {
        key: 'element',
        get: function get() {
            return this._element;
        },
        set: function set(value) {
            this._element = value;
        }
    }, {
        key: 'title',
        get: function get() {
            return this._title;
        },
        set: function set(value) {
            this._title = value;
        }
    }, {
        key: 'status',
        get: function get() {
            return this._status;
        },
        set: function set(value) {
            this._status = value;
        }
    }, {
        key: 'onBlockMessage',
        get: function get() {
            return this._onBlockMessage;
        },
        set: function set(value) {
            this._onBlockMessage = value;
        }
    }, {
        key: 'onUnBlockMessage',
        get: function get() {
            return this._onUnBlockMessage;
        },
        set: function set(value) {
            this._onUnBlockMessage = value;
        }
    }]);

    function AdminItemBlock(data) {
        _classCallCheck(this, AdminItemBlock);

        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._title = data.title;
        this._status = data.status;
        this._onBlockMessage = data.onBlockMessage;
        this._onUnBlockMessage = data.onUnBlockMessage;

        this.drawElement();
    }

    _createClass(AdminItemBlock, [{
        key: 'drawElement',
        value: function drawElement() {
            var self = this;
            this._element.append($('<a/>', {
                title: self._title,
                class: "but " + (self._status ? "block" : "unblock"),
                href: "javascript:void(0)",
                on: {
                    'click': function click(e) {
                        var element = $(this);
                        if (confirm(self._status ? self._onUnBlockMessage : self._onBlockMessage)) {
                            self.block(element);
                            nothing(e);
                        }
                    }
                }
            }));
        }
    }, {
        key: 'block',
        value: function block(element) {
            var self = this;
            bff.ajax(self._url, { id: self._itemID }, function (resp, err) {
                if (resp && resp.success) {
                    self._status = !self._status;
                    element.removeAttr('class');
                    element.addClass("but " + (self._status ? "block" : "unblock"));
                } else {
                    bff.errors.show(err, self);
                }
            });
        }
    }]);

    return AdminItemBlock;
}();
//# sourceMappingURL=AdminItemBlock.js.map
