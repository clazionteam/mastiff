'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AdminItemDelete = function () {
    _createClass(AdminItemDelete, [{
        key: 'url',
        get: function get() {
            return this._url;
        },
        set: function set(value) {
            this._url = value;
        }
    }, {
        key: 'itemID',
        get: function get() {
            return this._itemID;
        },
        set: function set(value) {
            this._itemID = value;
        }
    }, {
        key: 'element',
        get: function get() {
            return this._element;
        },
        set: function set(value) {
            this._element = value;
        }
    }, {
        key: 'contentElement',
        get: function get() {
            return this._contentElement;
        },
        set: function set(value) {
            this._contentElement = value;
        }
    }, {
        key: 'title',
        get: function get() {
            return this._title;
        },
        set: function set(value) {
            this._title = value;
        }
    }, {
        key: 'message',
        get: function get() {
            return this._message;
        },
        set: function set(value) {
            this._message = value;
        }
    }]);

    function AdminItemDelete(data) {
        _classCallCheck(this, AdminItemDelete);

        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._contentElement = data.contentElement;
        this._title = data.title;
        this._message = data.message;

        this.drawElement();
    }

    _createClass(AdminItemDelete, [{
        key: 'drawElement',
        value: function drawElement() {
            var self = this;
            this._element.append($('<a/>', {
                title: self._title,
                class: "but del item-del",
                href: "javascript:void(0)",
                on: {
                    'click': function click(e) {
                        var $this = $(this);
                        if (!self.status) if (confirm(self._message)) {
                            self.del();
                            nothing(e);
                        }
                    }
                }
            }));
        }
    }, {
        key: 'del',
        value: function del() {
            var self = this;
            bff.ajax(self._url, { id: self._itemID }, function (resp, err) {
                if (resp && resp.success) {
                    self._contentElement.remove();
                    window.location.reload();
                } else {
                    bff.errors.show(err, self);
                }
            });
        }
    }]);

    return AdminItemDelete;
}();
//# sourceMappingURL=AdminItemDelete.js.map
