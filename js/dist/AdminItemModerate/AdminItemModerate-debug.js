'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var AdminItemModerate = function () {
    _createClass(AdminItemModerate, [{
        key: 'url',
        get: function get() {
            return this._url;
        },
        set: function set(value) {
            this._url = value;
        }
    }, {
        key: 'itemID',
        get: function get() {
            return this._itemID;
        },
        set: function set(value) {
            this._itemID = value;
        }
    }, {
        key: 'element',
        get: function get() {
            return this._element;
        },
        set: function set(value) {
            this._element = value;
        }
    }, {
        key: 'title',
        get: function get() {
            return this._title;
        },
        set: function set(value) {
            this._title = value;
        }
    }, {
        key: 'status',
        get: function get() {
            return this._status;
        },
        set: function set(value) {
            this._status = value;
        }
    }, {
        key: 'message',
        get: function get() {
            return this._message;
        },
        set: function set(value) {
            this._message = value;
        }
    }]);

    function AdminItemModerate(data) {
        _classCallCheck(this, AdminItemModerate);

        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._title = data.title;
        this._status = data.status;
        this._message = data.message;

        this.drawElement();
    }

    _createClass(AdminItemModerate, [{
        key: 'drawElement',
        value: function drawElement() {
            var self = this;
            this._element.append($('<a/>', {
                title: self._title,
                class: "btn btn-mini btn-success success button" + (self._status ? " disabled" : ""),
                href: "javascript:void(0)",
                html: self._title,
                on: {
                    'click': function click(e) {
                        var $this = $(this);
                        if (!self._status) if (confirm(self._message)) {
                            self.moderate($this);
                            nothing(e);
                        }
                    }
                }
            }));
        }
    }, {
        key: 'moderate',
        value: function moderate(element) {
            var self = this;
            if (!self._status) bff.ajax(self._url, { id: self._itemID }, function (resp, err) {
                if (resp && resp.success) {
                    self._status = !self._status;
                    element.addClass('disabled');
                } else {
                    bff.errors.show(err, self);
                }
            });
        }
    }]);

    return AdminItemModerate;
}();
//# sourceMappingURL=AdminItemModerate.js.map
