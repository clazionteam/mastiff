'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CImagesUploaderTable = function () {
    _createClass(CImagesUploaderTable, [{
        key: 'data',
        get: function get() {
            return this._data;
        },
        set: function set(value) {
            this._data = value;
        }
    }, {
        key: 'url',
        get: function get() {
            return this._url;
        },
        set: function set(value) {
            this._url = value;
        }
    }, {
        key: 'form',
        get: function get() {
            return this._form;
        },
        set: function set(value) {
            this._form = value;
        }
    }, {
        key: 'progress',
        get: function get() {
            return this._progress;
        },
        set: function set(value) {
            this._progress = value;
        }
    }, {
        key: 'img',
        get: function get() {
            return this._img;
        },
        set: function set(value) {
            this._img = value;
        }
    }, {
        key: 'uploadButton',
        get: function get() {
            return this._uploadButton;
        },
        set: function set(value) {
            this._uploadButton = value;
        }
    }, {
        key: 'itemID',
        get: function get() {
            return this._itemID;
        },
        set: function set(value) {
            this._itemID = value;
        }
    }, {
        key: 'limit',
        get: function get() {
            return this._limit;
        },
        set: function set(value) {
            this._limit = value;
        }
    }, {
        key: 'uploaded',
        get: function get() {
            return this._uploaded;
        },
        set: function set(value) {
            this._uploaded = value;
        }
    }, {
        key: 'multiple',
        get: function get() {
            return this._multiple;
        },
        set: function set(value) {
            this._multiple = value;
        }
    }, {
        key: 'isEdit',
        get: function get() {
            return this._isEdit;
        },
        set: function set(value) {
            this._isEdit = value;
        }
    }, {
        key: 'isExternalSave',
        get: function get() {
            return this._isExternalSave;
        },
        set: function set(value) {
            this._isExternalSave = value;
        }
    }, {
        key: 'uploader',
        get: function get() {
            return this._uploader;
        },
        set: function set(value) {
            this._uploader = value;
        }
    }]);

    function CImagesUploaderTable(data) {
        _classCallCheck(this, CImagesUploaderTable);

        this._url = data.url;
        this._params = data.params;
        this._form = data.form;
        this._progress = data.progress;
        this._img = data.img;
        this._uploadButton = data.uploadButton;
        this._itemID = data.itemID;
        this._limit = data.limit;
        this._uploaded = data.uploaded;
        this._multiple = data.multiple;
        this._isEdit = data.isEdit;
        this._isExternalSave = data.isExternalSave;

        var self = this;

        // init uploader
        this._uploader = new qq.FileUploaderBasic({
            button: self._uploadButton.get(0),
            action: bff.ajaxURL(self._url, 'upload'),
            params: self._params,
            limit: self._limit,
            uploaded: self._uploaded,
            multiple: self._multiple,
            onSubmit: function onSubmit(id, fileName) {
                self._progress.show();
            },
            onComplete: function onComplete(id, fileName, resp) {
                if (resp && resp.success) {
                    self.onImageUpload(resp);
                } else {
                    if (resp.errors) {
                        bff.error(resp.errors);
                    }
                }
                if (!self._uploader.getInProgress()) {
                    self._progress.hide();
                }
                return true;
            }
        });

        this.initRotate(false);

        $('a[rel=wimg-group]', this._form).fancybox();

        if (this._isEdit) {
            var lostProcessed = false;
            $(window).bind('beforeunload', function () {
                if (!lostProcessed && id === 0) {
                    var $fn = $img.find('input.imgfn');
                    if ($fn.length > 0) {
                        lostProcessed = true;
                        var fn = [];
                        $fn.each(function () {
                            fn.push($(this).val());
                        });
                        self.delAll(false, fn);
                    }
                }
            });
        }
        this._data = data;
    }

    _createClass(CImagesUploaderTable, [{
        key: 'del',
        value: function del(imageID, imageFilename) {
            var self = this;
            bff.ajax(bff.ajaxURL(self._url, 'delete'), { image_id: imageID, filename: imageFilename }, function (resp) {
                if (resp && resp.success) {
                    self._form.find('li.wimg' + imageID).remove();
                    self._uploader.decrementUploaded();
                    self.initRotate(true);
                }
            }, self._progress);
            return false;
        }
    }, {
        key: 'delAll',
        value: function delAll(async, filenames) {
            var self = this;
            bff.ajax(bff.ajaxURL(self._url, 'delete-all'), { filenames: filenames }, function (resp) {
                if (resp && resp.success) {
                    self._img.empty();
                    self._uploader.resetUploaded();
                    self.initRotate(true);
                }
            }, self._progress, { async: async });
            return false;
        }
    }, {
        key: 'save',
        value: function save() {
            var self = this;
            bff.ajax(bff.ajaxURL(self._url, 'saveorder'), self._form.serialize(), function (resp) {
                if (resp.success) {
                    bff.success('Порядок успешно сохранен');
                }
            }, this._progress);
        }
    }, {
        key: 'initRotate',
        value: function initRotate(update) {
            var self = this;

            if (update === true) {
                self._img.sortable('refresh');
                $('a[rel=wimg-group]', self._img).fancybox();
                self.onPhotosCountChanged();
            } else {
                self._img.sortable();
            }
        }
    }, {
        key: 'onImageUpload',
        value: function onImageUpload(data) {
            var imageID = data.id;
            var self = this;

            if (self._uploader.getUploaded() == 0) self._img.find('li').remove();

            this._img.append($('<li/>', {
                class: 'wimg wimg' + imageID + ' relative left',
                html: [$('<input/>', { type: 'hidden', class: 'imgfn', name: 'img[' + imageID + ']', rel: imageID, value: data.filename }), $('<a/>', { href: data['v'], class: "thumbnail", rel: "wimg-group", html: $('<img/>', { src: data['s'] }) }), $('<a/>', { href: "javascript:void(0)", class: "but cross", text: "X", on: {
                        'click': function click(e) {
                            if (confirm('Удалить изображение?')) {
                                self.del(imageID, data.filename);
                                nothing(e);
                            }
                        }
                    } })]

            }));

            this.initRotate(true);
        }
    }, {
        key: 'onPhotosCountChanged',
        value: function onPhotosCountChanged() {
            var cnt = this._img.find('li').length;
            if (cnt > 0) {
                this._form.find('.nophoto-hide').show();
            } else {
                this._form.find('.nophoto-hide').hide();
            }
        }
    }]);

    return CImagesUploaderTable;
}();
//# sourceMappingURL=CImagesUploaderTable.js.map
