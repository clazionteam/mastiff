"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var MultilangInput = function () {
    function MultilangInput(params) {
        _classCallCheck(this, MultilangInput);

        this.elements = $("[data-multilang]");
        this.languages = params.languages;
        this.currentLanguage = params.currentLanguage;
        this.tabsArea = params.tabsArea;
        this.initialValues = params.initialValues;
        this.addTabs();
        this.modifyInputs();
        this.setInitValues();
    }

    _createClass(MultilangInput, [{
        key: "changeLang",
        value: function changeLang(self, source) {
            var element = $(source.currentTarget);
            var elements = $(".multilang-input");
            elements.each(function (index, item) {
                item = $(item);
                if (item.attr("data-lang") != element.attr("data-tab-lang")) item.hide();else {
                    item.show();
                    self.currentLanguage = element.attr("data-tab-lang");
                }
            });
        }
    }, {
        key: "addTabs",
        value: function addTabs() {
            var self = this;
            self.languages.forEach(function (lang) {
                var tabClass = "multilang-tab";
                var attr = {};
                if (lang == self.currentLanguage) {
                    tabClass = "multilang-tab-active";
                    attr.checked = 'checked';
                }
                self.tabsArea.append([$('<input/>', {
                    class: 'multilang-tab-active multilang-tab-' + lang,
                    type: 'radio',
                    name: 'multilang-tab',
                    id: 'multilang-tab-' + lang,
                    click: function click(source) {
                        return self.changeLang(self, source);
                    },
                    "data-tab-lang": lang,
                    attr: attr
                }), $('<label/>', {
                    for: 'multilang-tab-' + lang,
                    html: lang
                })]);
            });
        }
    }, {
        key: "modifyInputs",
        value: function modifyInputs() {
            var self = this;
            self.elements.each(function (index, item) {
                item = $(item);
                var itemName = item.attr('name');
                var isArray = typeof item.attr('data-array') !== 'undefined';
                item.attr('name', itemName + '[' + self.currentLanguage + ']' + (isArray ? '[]' : ''));
                item.addClass('multilang-input');
                item.attr("data-lang", self.currentLanguage);
                var attr = {};
                if (isArray) {
                    attr['data-array'] = 'data-array';
                }

                self.languages.forEach(function (lang) {
                    if (lang != self.currentLanguage) {
                        itemName = itemName + '[' + lang + ']' + (isArray ? '[]' : '');
                        item.after($('<' + item.prop("tagName") + '/>', {
                            name: itemName,
                            type: item.attr('type'),
                            class: 'multilang-input',
                            style: 'display: none',
                            readonly: item.attr('readonly'),
                            "data-lang": lang,
                            attr: attr
                        }));
                    }
                });
            });
        }
    }, {
        key: "convertJsonToName",
        value: function convertJsonToName(obj) {
            var _this = this;

            var res = {};

            var _loop = function _loop(i) {
                if (!obj.hasOwnProperty(i)) return "continue";
                if (typeof obj[i] === "string") res[i] = obj[i];else if (_typeof(obj[i]) === "object" && obj[i] != null) {
                    _this.languages.forEach(function (lang) {
                        if (obj[i].hasOwnProperty(lang)) {
                            res[i + '[' + lang + ']'] = obj[i][lang];
                        }
                    });
                }
            };

            for (var i in obj) {
                var _ret = _loop(i);

                if (_ret === "continue") continue;
            }
            return res;
        }
    }, {
        key: "setInitValues",
        value: function setInitValues() {
            var self = this;
            var initialValues = this.convertJsonToName(self.initialValues);
            var elements = $(".multilang-input");
            elements.each(function (index, item) {
                item = $(item);
                var name = item.attr('name');
                var isArray = typeof item.attr('data-array') !== 'undefined';
                if (!isArray) {
                    if (typeof initialValues[name] !== 'undefined') {
                        if (initialValues[name] != '') {
                            item.val(initialValues[name]);
                        }
                    }
                } else {
                    if (typeof initialValues[name.substr(0, name.length - 2)] !== 'undefined') {
                        if (initialValues[name.substr(0, name.length - 2)].length > 0) {
                            initialValues[name.substr(0, name.length - 2)].forEach(function (i) {
                                item.after($('<' + item.prop("tagName") + '/>', {
                                    name: item.attr('name'),
                                    type: item.attr('type'),
                                    class: 'multilang-input',
                                    readonly: item.attr('readonly'),
                                    "data-lang": item.attr('data-lang'),
                                    value: i,
                                    style: item.attr('style')
                                }));
                            });
                            item.remove();
                        }
                    }
                }
            });
        }
    }, {
        key: "getCurrentLanguage",
        value: function getCurrentLanguage() {
            return this.currentLanguage;
        }
    }]);

    return MultilangInput;
}();
//# sourceMappingURL=MultilangInput.js.map
