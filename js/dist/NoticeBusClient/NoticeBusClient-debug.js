"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var NoticeBusClient = function () {
    function NoticeBusClient(data) {
        var _this = this;

        _classCallCheck(this, NoticeBusClient);

        this.socket = new WebSocket(data.endpoint);
        this.socket.onopen = function () {
            _this.sendMessage(data.token);
        };
        this.socket.onclose = data.onClose;
        this.socket.onerror = data.onError;
        this.socket.onmessage = data.onMessage;
    }

    _createClass(NoticeBusClient, [{
        key: "sendMessage",
        value: function sendMessage(msg) {
            this.socket.send(msg);
        }
    }]);

    return NoticeBusClient;
}();
//# sourceMappingURL=NoticeBusClient.js.map
