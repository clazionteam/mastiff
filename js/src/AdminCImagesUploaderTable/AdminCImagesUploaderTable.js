'use strict';

class AdminCImagesUploaderTable {
    get data() {
        return this._data;
    }

    set data(value) {
        this._data = value;
    }

    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

    get form() {
        return this._form;
    }

    set form(value) {
        this._form = value;
    }

    get progress() {
        return this._progress;
    }

    set progress(value) {
        this._progress = value;
    }

    get img() {
        return this._img;
    }

    set img(value) {
        this._img = value;
    }

    get uploadButton() {
        return this._uploadButton;
    }

    set uploadButton(value) {
        this._uploadButton = value;
    }

    get itemID() {
        return this._itemID;
    }

    set itemID(value) {
        this._itemID = value;
    }

    get limit() {
        return this._limit;
    }

    set limit(value) {
        this._limit = value;
    }

    get uploaded() {
        return this._uploaded;
    }

    set uploaded(value) {
        this._uploaded = value;
    }

    get multiple() {
        return this._multiple;
    }

    set multiple(value) {
        this._multiple = value;
    }

    get isEdit() {
        return this._isEdit;
    }

    set isEdit(value) {
        this._isEdit = value;
    }

    get isExternalSave() {
        return this._isExternalSave;
    }

    set isExternalSave(value) {
        this._isExternalSave = value;
    }

    get uploader() {
        return this._uploader;
    }

    set uploader(value) {
        this._uploader = value;
    }
    constructor(data) {
        this._url = data.url;
        this._form = data.form;
        this._progress = data.progress;
        this._img = data.img;
        this._uploadButton = data.uploadButton;
        this._itemID = data.itemID;
        this._limit = data.limit;
        this._uploaded = data.uploaded;
        this._multiple = data.multiple;
        this._isEdit = data.isEdit;
        this._isExternalSave = data.isExternalSave;

        const self = this;

        // init uploader
        this._uploader = new qq.FileUploaderBasic({
            button: self._uploadButton.get(0),
            action: self._url + 'upload',
            limit: self._limit,
            uploaded: self._uploaded,
            multiple: self._multiple,
            onSubmit: function (id, fileName) {
                self._progress.show();
            },
            onComplete: function (id, fileName, resp) {
                if (resp && resp.success) {
                    self.onImageUpload(resp);
                } else {
                    if (resp.errors) {
                        bff.error(resp.errors);
                    }
                }
                if (!self._uploader.getInProgress()) {
                    self._progress.hide();
                }
                return true;
            }
        });

        this.initRotate(false);

        $('a[rel=wimg-group]', this._form).fancybox();

        if(this._isEdit) {
            let lostProcessed = false;
            $(window).bind('beforeunload', function(){
                if(!lostProcessed && id===0) {
                    let $fn = $img.find('input.imgfn');
                    if($fn.length > 0) {
                        lostProcessed = true;
                        let fn = [];
                        $fn.each(function(){
                            fn.push($(this).val());
                        });
                        self.delAll(false, fn);
                    }
                }
            });
        }
        this._data = data;
    }

    del(imageID, imageFilename) {
        const self = this;
        bff.ajax(self._url+'delete',{image_id: imageID, filename: imageFilename}, function(resp){
            if(resp && resp.success) {
                self._form.find('li.wimg'+imageID).remove();
                self._uploader.decrementUploaded();
                self.initRotate(true);
            }
        }, self._progress);
        return false;
    }

    delAll(async, filenames) {
        const self = this;
        bff.ajax(self._url+'delete-all', {filenames: filenames}, function(resp){
            if(resp && resp.success) {
                self._img.empty();
                self._uploader.resetUploaded();
                self.initRotate(true);
            }
        }, self._progress, {async: async});
        return false;
    }

    save() {
        const self = this;
        bff.ajax(self._url+'saveorder', self._form.serialize(), function(resp){
            if(resp.success) {
                bff.success('Порядок успешно сохранен');
            }
        }, this._progress);
    }

    initRotate(update) {
        const self = this;

        if(update === true) {
            self._img.sortable('refresh');
            $('a[rel=wimg-group]', self._img).fancybox();
            self.onPhotosCountChanged();
        } else {
            self._img.sortable();
        }
    }

    onImageUpload(data) {
        let imageID = data.id;
        const self = this;

        if(self._uploader.getUploaded() == 0) self._img.find('li').remove();

        this._img.append($(
            '<li/>', {
                class:'wimg wimg' + imageID + ' relative left',
                html: [
                    $('<input/>', { type: 'hidden', class: 'imgfn', name: 'img['+imageID+']', rel: imageID, value: data.filename}),
                    $('<a/>', {href: data['v'], class: "thumbnail", rel: "wimg-group", html: $('<img/>', { src: data['s']})}),
                    $('<a/>', {href: "javascript:void(0)", class: "but cross", style: "position: absolute;right:2px;top:7px;", on:{
                        'click': function (e) {
                            if(confirm('Удалить изображение?')) {
                                self.del(imageID, data.filename);
                                nothing(e);
                            }
                        }
                    }})
                ],

            }
        ));

        this.initRotate(true);
    }

    onPhotosCountChanged() {
        let cnt = this._img.find('li').length;
        if(cnt > 0) {
            this._form.find('.nophoto-hide').show();
        } else {
            this._form.find('.nophoto-hide').hide();
        }
    }
}
