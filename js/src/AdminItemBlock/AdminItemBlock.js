'use strict';

class AdminItemBlock {
    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

    get itemID() {
        return this._itemID;
    }

    set itemID(value) {
        this._itemID = value;
    }

    get element() {
        return this._element;
    }

    set element(value) {
        this._element = value;
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get onBlockMessage() {
        return this._onBlockMessage;
    }

    set onBlockMessage(value) {
        this._onBlockMessage = value;
    }

    get onUnBlockMessage() {
        return this._onUnBlockMessage;
    }

    set onUnBlockMessage(value) {
        this._onUnBlockMessage = value;
    }
    constructor(data) {
        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._title = data.title;
        this._status = data.status;
        this._onBlockMessage = data.onBlockMessage;
        this._onUnBlockMessage = data.onUnBlockMessage;

        this.drawElement();

    }
    drawElement() {
        const self = this;
        this._element.append($(
            '<a/>', {
                title: self._title,
                class: "but " + (self._status ? "block" : "unblock"),
                href: "javascript:void(0)",
                on: {
                    'click': function (e) {
                        const element = $(this);
                        if (confirm(self._status ? self._onUnBlockMessage : self._onBlockMessage)) {
                            self.block(element);
                            nothing(e);
                        }
                    }
                }
            }
        ));
    }

    block(element) {
        const self = this;
        bff.ajax(self._url, {id: self._itemID}, function(resp, err) {
           if(resp && resp.success) {
               self._status = !self._status;
               element.removeAttr('class');
               element.addClass("but " + (self._status ? "block" : "unblock"))
           } else {
               bff.errors.show(err, self);
           }
        });
    }
}