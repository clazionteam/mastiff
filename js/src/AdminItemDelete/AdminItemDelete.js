'use strict';

class AdminItemDelete {
    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

    get itemID() {
        return this._itemID;
    }

    set itemID(value) {
        this._itemID = value;
    }

    get element() {
        return this._element;
    }

    set element(value) {
        this._element = value;
    }

    get contentElement() {
        return this._contentElement;
    }

    set contentElement(value) {
        this._contentElement = value;
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    get message() {
        return this._message;
    }

    set message(value) {
        this._message = value;
    }
    constructor(data) {
        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._contentElement = data.contentElement;
        this._title = data.title;
        this._message = data.message;

        this.drawElement();
    }
    drawElement() {
        const self = this;
        this._element.append($(
            '<a/>', {
                title: self._title,
                class: "but del item-del",
                href: "javascript:void(0)",
                on: {
                    'click': function (e) {
                        const $this = $(this);
                        if(!self.status)
                            if (confirm(self._message)) {
                                self.del();
                                nothing(e);
                            }
                    }
                }
            }
        ));
    }

    del() {
        const self = this;
        bff.ajax(self._url, {id: self._itemID}, function(resp, err) {
           if(resp && resp.success) {
               self._contentElement.remove();
               window.location.reload();
           } else {
               bff.errors.show(err, self);
           }
        });
    }
}