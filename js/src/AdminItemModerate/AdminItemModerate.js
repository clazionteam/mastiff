'use strict';

class AdminItemModerate {
    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

    get itemID() {
        return this._itemID;
    }

    set itemID(value) {
        this._itemID = value;
    }

    get element() {
        return this._element;
    }

    set element(value) {
        this._element = value;
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    get status() {
        return this._status;
    }

    set status(value) {
        this._status = value;
    }

    get message() {
        return this._message;
    }

    set message(value) {
        this._message = value;
    }
    constructor(data) {
        this._url = data.url;
        this._itemID = data.id;
        this._element = data.element;
        this._title = data.title;
        this._status = data.status;
        this._message = data.message;

        this.drawElement();
    }
    drawElement() {
        const self = this;
        this._element.append($(
            '<a/>', {
                title: self._title,
                class: "btn btn-mini btn-success success button" + (self._status ? " disabled" : ""),
                href: "javascript:void(0)",
                html: self._title,
                on: {
                    'click': function (e) {
                        const $this = $(this);
                        if(!self._status)
                            if (confirm(self._message)) {
                                self.moderate($this);
                                nothing(e);
                            }
                    }
                }
            }
        ));
    }

    moderate(element) {
        const self = this;
        if(!self._status)
            bff.ajax(self._url, {id: self._itemID}, function(resp, err) {
               if(resp && resp.success) {
                   self._status = !self._status;
                   element.addClass('disabled');
               } else {
                   bff.errors.show(err, self);
               }
            });
    }
}