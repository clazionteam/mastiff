class MultilangInput {
    constructor(params) {
        this.elements = $("[data-multilang]");
        this.languages = params.languages;
        this.currentLanguage = params.currentLanguage;
        this.tabsArea = params.tabsArea;
        this.initialValues = params.initialValues;
        this.addTabs();
        this.modifyInputs();
        this.setInitValues();
    }

    changeLang(self, source) {
        const element = $(source.currentTarget);
        const elements = $(".multilang-input");
        elements.each((index, item) => {
            item = $(item);
            if(item.attr("data-lang") != element.attr("data-tab-lang"))
                item.hide();
            else {
                item.show();
                self.currentLanguage = element.attr("data-tab-lang");
            }
        })
    }

    addTabs() {
        const self = this;
        self.languages.forEach((lang) => {
            let tabClass = "multilang-tab";
            let attr = {};
            if (lang == self.currentLanguage) {
                tabClass = "multilang-tab-active";
                attr.checked = 'checked';
            }
            self.tabsArea.append(
                [
                    $('<input/>', {
                        class: 'multilang-tab-active multilang-tab-' + lang,
                        type: 'radio',
                        name: 'multilang-tab',
                        id: 'multilang-tab-' + lang,
                        click: (source) => self.changeLang(self, source),
                        "data-tab-lang": lang,
                        attr: attr
                    }),
                    $('<label/>', {
                        for: 'multilang-tab-' + lang,
                        html: lang,
                    })
                ]
            );
        });
    }

    modifyInputs() {
        const self = this;
        self.elements.each((index, item) => {
            item = $(item);
            let itemName = item.attr('name');
            const isArray = typeof item.attr('data-array') !== 'undefined';
            item.attr('name',
                itemName
                + '[' + self.currentLanguage + ']'
                + (isArray ? '[]' : ''));
            item.addClass('multilang-input');
            item.attr("data-lang", self.currentLanguage);
            let attr = {};
            if(isArray) {
                attr['data-array'] = 'data-array';
            }

            self.languages.forEach((lang) => {
                if (lang != self.currentLanguage) {
                    itemName =
                        itemName
                        + '[' + lang + ']'
                        + (isArray ? '[]' : '');
                    item.after($(
                        '<'+ item.prop("tagName") +'/>', {
                            name: itemName,
                            type: item.attr('type'),
                            class: 'multilang-input',
                            style: 'display: none',
                            readonly: item.attr('readonly'),
                            "data-lang": lang,
                            attr: attr
                        }
                    ));
                }
            });
        });
    }

    convertJsonToName(obj) {
        let res = {};
        for(let i in obj) {
            if(!obj.hasOwnProperty(i)) continue;
            if(typeof obj[i] === "string")
                res[i] = obj[i];
            else if(typeof obj[i] === "object" && obj[i] != null) {
                this.languages.forEach((lang) => {
                    if((obj[i]).hasOwnProperty(lang)) {
                        res[i + '['+lang+']'] = obj[i][lang];
                    }
                })
            }

        }
        return res;
    }

    setInitValues() {
        const self = this;
        const initialValues = this.convertJsonToName(self.initialValues);
        const elements = $(".multilang-input");
        elements.each((index, item) => {
            item = $(item);
            const name = item.attr('name');
            const isArray = typeof item.attr('data-array') !== 'undefined';
            if(!isArray) {
                if (typeof initialValues[name] !== 'undefined') {
                    if (initialValues[name] != '') {
                        item.val(initialValues[name]);
                    }
                }
            } else {
                if(typeof initialValues[name.substr(0, name.length - 2)] !== 'undefined') {
                    if(initialValues[name.substr(0, name.length - 2)].length > 0) {
                        initialValues[name.substr(0, name.length - 2)].forEach((i) => {
                            item.after($(
                                '<' + item.prop("tagName") + '/>', {
                                    name: item.attr('name'),
                                    type: item.attr('type'),
                                    class: 'multilang-input',
                                    readonly: item.attr('readonly'),
                                    "data-lang": item.attr('data-lang'),
                                    value: i,
                                    style: item.attr('style')
                                }
                            ));
                        });
                        item.remove();
                    }
                }
            }
        });

    }

    getCurrentLanguage() {
        return this.currentLanguage;
    }
}
