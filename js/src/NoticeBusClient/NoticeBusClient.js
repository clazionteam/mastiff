class NoticeBusClient {
    constructor(data) {
        this.socket = new WebSocket(data.endpoint);
        this.socket.onopen = () => {
            this.sendMessage(data.token);
        };
        this.socket.onclose = data.onClose;
        this.socket.onerror = data.onError;
        this.socket.onmessage = data.onMessage;
    }

    sendMessage(msg) {
            this.socket.send(msg);
    }
}
