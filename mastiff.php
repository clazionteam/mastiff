#!/usr/bin/env php
<?php
//    require_once 'autoload.php';
    require_once 'autoload.php';

    use Symfony\Component\Console\Application;
    use Mastiff\Commands\TamarangaInstall;

    $app = new Application();
    $app->add(new TamarangaInstall());
    $app->run();

?>