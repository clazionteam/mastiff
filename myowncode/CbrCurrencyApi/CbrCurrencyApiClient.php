<?php

namespace mastiff\myowncode\CbrCurrencyApi;

class CbrCurrencyApiClient extends \Module
{
    protected static $instance;
    /**
     * Cron: получения курса валют
     * Рекомендуемый период: раз в сутки
     */
    public function cronCurrency()
    {
        $xml = file_get_contents(\config::sys('currency.api.path'));
        $currency = new \SimpleXMLElement($xml);
        $currenciesToUpdate = \config::sys('currency.to.update');
        foreach($currenciesToUpdate as $k => $c) {
            $result = $currency->xpath("//Valute[@ID='" . $c['id'] . "']");
            if(!empty($result)) {
                $result = reset($result);
                $rate = self::tofloat($result->Value) / self::tofloat($result->Nominal);
                $rate += $rate / 100 * $c['percent'];
                $this->db->update(TABLE_CURRENCIES, array('rate' => $rate), array('id' => $k));
            }
        }
    }

    public function __construct()
    {
        parent::init();
    }

    public static function create()
    {
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getCurrencies() {
        $currencies = \config::sys('currency.in.header');
        $currencies = '(' . join(',', $currencies) . ')';
        return $this->db->select(
            "SELECT C.*, CL.title_short FROM "
            . TABLE_CURRENCIES . " C INNER JOIN "
            . TABLE_CURRENCIES_LANG . " CL USING(id) WHERE C.id IN " . $currencies
        );
    }

    /**
     * Приводит строку к float
     * @param $num string
     * @return float
     */
    public static function tofloat($num) {
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $num));
        }

        return floatval(
            preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
}