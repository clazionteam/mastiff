<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Class BaseSignal
 * abstract signal
 * @package mastiff\myowncode\EventSystem
 */
abstract class BaseSignal implements Signal
{
    /**
     * @var string|int name of the slot
     */
    protected $name;
    /**
     * @var mixed result of the execution
     */
    protected $result;

    /**
     * @var array any additional data
     */
    protected $additionalData;

    /**
     * @return int get slot name
     */
    public function getSlotName() {
        return $this->name;
    }

    /**
     * @param $name slot name
     */
    public function setSlotName($name) {
        $this->name = $name;
    }

    /**
     * @return mixed get result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $value set result
     */
    public function setResult($value)
    {
        $this->result = $value;
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value) {
        $this->additionalData[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name) {
        return $this->additionalData[$name];
    }
}