<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Class Event
 * signal emitting and signal processing
 * @package mastiff\myowncode\EventSystem
 */
class Event
{
    /**
     * @var Event | null
     */
    private static $instance = null;

    /**
     * @var SlotList
     */
    private static $oSlotList;

    /**
     * Event constructor.
     * @param SlotList $oSlotList
     */
    private function __construct(SlotList $oSlotList)
    {
        self::$oSlotList = $oSlotList;
    }

    /**
     * @param SlotList $oSlotList
     * @return Event
     */
    public static function initialize(SlotList $oSlotList)
    {
        if (is_null(self::$instance))
            self::$instance = new self($oSlotList);
        return self::$instance;
    }

    /**
     * @param Signal $signal
     * @return mixed
     */
    public static function emit(Signal &$signal)
    {
        $slot = self::$oSlotList->get($signal->getSlotName());
        return $slot->receive($signal);
    }
}