<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Interface Signal
 * @package mastiff\myowncode\EventSystem
 */
interface Signal
{
    /**
     * @return mixed
     */
    function getSlotName();

    /**
     * @param $name
     * @return mixed
     */
    function setSlotName($name);

    /**
     * @return mixed
     */
    function getResult();

    /**
     * @param $value
     * @return mixed
     */
    function setResult($value);

    /**
     * @param $name
     * @param $value
     * @return mixed
     */
    function __set($name, $value);

    /**
     * @param $name
     * @return mixed
     */
    function __get($name);
}