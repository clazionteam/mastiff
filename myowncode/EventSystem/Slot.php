<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Class Slot
 * @package mastiff\myowncode\EventSystem
 */
class Slot
{
    /**
     * @var string|int slot name
     */
    protected $name;
    /**
     * @var callable
     */
    protected $callback;

    /**
     * Slot constructor.
     * @param string $name
     * @param callable $callback
     */
    public function __construct($name, $callback)
    {
        $this->setName($name);
        $this->setCallback($callback);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param callable $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    public function receive(Signal &$signal)
    {
        return call_user_func($this->callback, $signal);
    }
}