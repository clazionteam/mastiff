<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Class SlotList
 * container for slots
 * @package mastiff\myowncode\EventSystem
 */
class SlotList implements \Iterator
{

    /**
     * @var array slots
     */
    private $slots;

    /**
     * @var int current index
     */
    private $index = 0;

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        return $this->slots[$this->index];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        ++$this->index;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->index;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return isset($this->slots[$this->index]);
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->index = 0;
    }

    /**
     * @param Slot $slot
     */
    public function register(Slot $slot)
    {
        $this->slots[$slot->getName()] = $slot;
    }

    /**
     * get slot by key
     * @param string|int $key slot key
     * @return Slot
     */
    public function get($key)
    {
        return $this->slots[$key];
    }

    /**
     * set slot by key
     * @param string|int $key $key slot key
     * @param Slot $value
     */
    public function set($key, $value)
    {
        $this->slots[$key] = $value;
    }

}