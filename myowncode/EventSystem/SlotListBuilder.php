<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Interface SlotListBuilder
 * @package mastiff\myowncode\EventSystem
 */
interface SlotListBuilder
{
    /**
     * @param Slot $slot slot
     */
    function addSlot(Slot $slot);

    /**
     * @return SlotList slot container
     */
    function getList();
}