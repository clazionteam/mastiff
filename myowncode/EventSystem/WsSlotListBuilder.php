<?php

namespace mastiff\myowncode\EventSystem;

/**
 * Class WsSlotListBuilder
 * @package mastiff\myowncode\EventSystem
 */
class WsSlotListBuilder implements SlotListBuilder
{
    /**
     * @var SlotList
     */
    private $slotList;

    /**
     * WsSlotListBuilder constructor.
     */
    public function __construct()
    {
        $this->slotList = new SlotList();
    }

    /**
     * @inheritdoc
     */
    public function addSlot(Slot $slot)
    {
        $this->slotList->register($slot);
    }

    /**
     * @inheritdoc
     */
    public function getList()
    {
        return $this->slotList;
    }
}