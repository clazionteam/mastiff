<?php
namespace mastiff\myowncode\NoticeBus;

use GuzzleHttp\Client;


class NoticeBusApiClient
{
    protected $endpoint;
    protected $username;
    protected $password;
    protected $token;

    protected $smemid;

    protected $client;

    private static $instance = null;

    private function __construct($endpoint, $username, $password, $smemid, $tokensize = 32)
    {
        $this->endpoint = $endpoint;
        $this->username = $username;
        $this->password = $password;
        $this->smemid = $smemid;

        $this->client = new Client([
            'base_uri' => $endpoint,
            'timeout' => 1.0
        ]);
        try {
            // TODO Fix this
            @$this->readToken($tokensize);
        } catch (\Exception $e) {
            \bff::log($e->getMessage());
        }
        if(empty($this->token))
        try {
            $this->authenticate();
        } catch (\Exception $e) {
            \bff::log($e->getMessage());
        }
    }


    public static function create($endpoint, $username, $password, $smemid, $tokensize = 32)
    {
        if(is_null(self::$instance))
            self::$instance = new self($endpoint, $username, $password, $smemid, $tokensize);
        return self::$instance;
    }

    private function sendRequest($url, array $data, $token = null, $tries = 2)
    {
        if($tries <= 0) return null;
        $headers = ['content-type' => 'application/json'];
        if(!is_null($token)) {
            $headers['Authorization'] = 'Token ' . $token;
        }
        $response = null;
        try {
            $response = $this->client->post($url,
                [
                    'body' => \GuzzleHttp\json_encode($data),
                    'headers' => $headers,
                    'content-type' => 'application/json'
                ]
            );
            return $response->getBody()->getContents();
        } catch( \GuzzleHttp\Exception\ClientException $e ) {
            $this->authenticate();
            $this->sendRequest($url, $data, $token, --$tries);
        }
        throw new \Exception('Can not make request - maximum tries reached');
    }

    /**
     * @param $userId integer id пользователя
     * @param $message string сообщение
     * @return bool result
     */
    public function sendMessage($userId, $message)
    {
        $result = $this->sendRequest('WebSocket', ['userid' => $userId, 'message' => $message], $this->token);
        return \GuzzleHttp\json_decode($result);
    }

    /**
     * @param $userId integer id пользователя
     * @return string user token
     */
    public function registerUser($userId)
    {
        $token = $this->sendRequest('WebSocketSession', ['userid' => $userId], $this->token);
        return \GuzzleHttp\json_decode($token)->token;
    }

    /**
     * Authenticate client
     */
    public function authenticate()
    {
        $token = $this->sendRequest('Authorization', [
            'username' => $this->username,
            'password' => $this->password
        ]);

        $this->token = \GuzzleHttp\json_decode($token)->token;
        $this->writeToken();
    }

    protected function writeToken()
    {
        if(sizeof($this->token) == 0) return;

        $shared_mem = shmop_open($this->smemid , "c" , 0644 , strlen($this->token));
        shmop_write($shared_mem, $this->token, 0);
        shmop_close($shared_mem);
    }

    protected function readToken($tokensize)
    {
        $shared_mem = shmop_open($this->smemid , "a" , 0644 , $tokensize);
        $this->token = shmop_read($shared_mem, 0, $tokensize);
        shmop_close($shared_mem);
    }

    public static function websocketToken()
    {
        $socket=self::create(
            \config::sys('noticebus.endpoint'),
            \config::sys('noticebus.user'),
            \config::sys('noticebus.pass'),
            \config::sys('noticebus.sharedmemid')
        );
        if((bool)\User::id())
            return $socket->registerUser(\User::id());
        return null;
    }
}
