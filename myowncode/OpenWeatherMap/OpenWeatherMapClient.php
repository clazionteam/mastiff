<?php

namespace mastiff\myowncode\OpenWeatherMap;

use GuzzleHttp\Client;


class OpenWeatherMapClient
{
    const endpoint = "http://api.openweathermap.org/data/2.5/weather";

    protected $token;
    protected $client;
    private static $instance = null;

    protected function __construct()
    {
        $this->token = \config::sys('openweathermap.token');
        $this->client = $this->client = new Client([
            'base_uri' => self::endpoint,
            'timeout' => 5.0
        ]);
    }

    public static function create()
    {
        if(is_null(self::$instance))
            self::$instance = new self();
        return self::$instance;
    }

    public function getWeatherLonLat($lon, $lat)
    {
        try {
            $res = $this->client->get('', ['query' => ['lon' => $lon, 'lat' => $lat, 'appid' => $this->token]]);
            return \GuzzleHttp\json_decode($res->getBody()->getContents());
        } catch (\Exception $e) {
            return null;
        }

    }

    public function kelvinToCelsius($temp)
    {
        if(is_null($temp))
            return null;
        return floatval($temp) - 273.15;
    }
}