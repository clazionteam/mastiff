<?php

namespace mastiff\myowncode\StaticText;

class Base
{
    private static $instance = null;
    private $_config = null;

    protected function __construct()
    {
        $this->_config = require(PATH_BASE . 'config/static.text.php');
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function getStaticText($key)
    {
        if (key_exists($key, $this->_config)) {
            return $this->_config[$key];
        }
        return '';
    }
}