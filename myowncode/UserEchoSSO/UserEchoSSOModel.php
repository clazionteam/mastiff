<?php

namespace mastiff\myowncode\UserEchoSSO;


class UserEchoSSOModel implements \JsonSerializable
{
    private $guid = null;
    private $expires = null;
    private $displayName = null;
    private $email = null;
    private $locale = null;
    private $avatar_url = null;

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param mixed $expires
     * @return $this
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param mixed $displayName
     * @return $this
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param mixed $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvatarUrl()
    {
        return $this->avatar_url;
    }

    /**
     * @param mixed $avatar_url
     * @return $this
     */
    public function setAvatarUrl($avatar_url)
    {
        $this->avatar_url = $avatar_url;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return \GuzzleHttp\json_encode([
            "guid" => $this->getGuid(),
            "expires" => $this->getExpires(),
            "display_name" => $this->getDisplayName(),
            "email" => $this->getEmail(),
            "locale" => $this->getLocale(),
            "avatar_url" => $this->getAvatarUrl()
        ]);
    }

    function toArray()
    {
        return [
            "guid" => $this->getGuid(),
            "expires" => $this->getExpires(),
            "display_name" => $this->getDisplayName(),
            "email" => $this->getEmail(),
            "locale" => $this->getLocale(),
            "avatar_url" => $this->getAvatarUrl()
        ];
    }
}