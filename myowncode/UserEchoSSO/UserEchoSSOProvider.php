<?php

namespace mastiff\myowncode\UserEchoSSO;

class UserEchoSSOProvider
{
    /**
     * @return string
     */
    public static function SSOAuth() {
        $oUser = static::SSOModel();
        if(is_null($oUser)) return '';
        $sSsoToken = (new UeSsoCipher())->encrypt(\config::sys('userecho.sso.key'), $oUser->toArray());
        return $sSsoToken;
    }

    /**
     * @return UserEchoSSOModel|null
     */
    public static function SSOModel() {
        $aUserData = \User::data(['id', 'login', 'email', 'avatar']);
        if(empty($aUserData))
            return null;
        return (new UserEchoSSOModel())
            ->setGuid($aUserData['id'])
            ->setDisplayName($aUserData['login'])
            ->setEmail($aUserData['email'])
            ->setExpires(time() + \config::sys('userecho.sso.expires', TYPE_UINT))
            ->setLocale(LNG)
            ->setAvatarUrl(\Request::scheme() . ':' . \UsersAvatar::url($aUserData['id'], $aUserData['avatar'], \UsersAvatar::szSmall));
    }
}