<?php

namespace Mastiff\Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class TamarangaInstall extends Command
{
    private $configTemplate = "
<?php
    return [
        'site.host' => '%s',
        'site.static' => '//%s',
        # db
        'db.host' => '%s',
        'db.port' => '%d',
        'db.name' => '%s',
        'db.user' => '%s',
        'db.pass' => '%s',
        # debug
        'php.errors.reporting' => -1, //all
        'php.errors.display' => 1,
        'debug' => true,
    ];";

    private $appDir = '..' . DIRECTORY_SEPARATOR . 'mastiffApp';

    private $configFile = '..' . DIRECTORY_SEPARATOR .  'config' . DIRECTORY_SEPARATOR . 'sys-local.php';

    private $defaultDbHost = '127.0.0.1';
    private $defaultDbPort = 3306;

    private $defaultDumpFile = '../install/install.sql';

    protected function configure()
    {
        parent::configure();
        $this->setName('tamaranga:install')
            ->setDescription("Install database from dump")
            ->setDefinition(
                [
                    new InputOption('file', 'f', InputOption::VALUE_OPTIONAL, 'Path to database dump'),
                    new InputOption('host', 'H', InputOption::VALUE_OPTIONAL, 'Databse host'),
                    new InputOption('port', 'p', InputOption::VALUE_OPTIONAL, 'Databse port'),
                    new InputOption('user', 'U', InputOption::VALUE_REQUIRED, 'Database user name'),
                    new InputOption('password', 'P', InputOption::VALUE_OPTIONAL, 'Database user password'),
                    new InputOption('db', 'd', InputOption::VALUE_REQUIRED, 'Database name'),
                    new InputOption('sitehost', 's', InputOption::VALUE_REQUIRED, 'Website host'),
                ]
            );
    }

    private function recurseCopy($src, $dst)
    {
        $dir = opendir($src);
        mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurseCopy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $headerStyle = new OutputFormatterStyle('white', 'green', ['bold']);
        $output->getFormatter()->setStyle('header', $headerStyle);

        $errorStyle = new OutputFormatterStyle('white', 'red', ['bold']);
        $output->getFormatter()->setStyle('error', $errorStyle);

        $user = $input->getOption('user');
        $password = $input->getOption('password');
        $host = $input->getOption('host');
        $file = $input->getOption('file');
        $port = $input->getOption('port');
        $db = $input->getOption('db');
        $sitehost = $input->getOption('sitehost');

        if(!$file)
            $file = $this->defaultDumpFile;

        if(!$host)
            $host = $this->defaultDbHost;

        if(!$port)
            $port = $this->defaultDbPort;

        $file = realpath($file);

        if(!$password) {
            $command = "mysql -h$host -u$user -P$port -s $db < $file";
        } else {
            $command = "mysql -h$host -u$user -p$password -P$port -s $db < $file";
        }


        $output->writeln('<header>Database restoring...</header>');

        $result = shell_exec($command);
        if(empty($result)) {
            $output->writeln('<header>OK</header>');
        } else {
            $output->writeln(
                '<error>There was error during db restore. Check username and password. Read database logs.</error>'
            );
            return;
        }


        $output->writeln('<header>Config writing...</header>');

        $config = fopen($this->configFile, "w");
        fwrite($config, sprintf($this->configTemplate, $sitehost, $sitehost, $host, $port, $db, $user, $password));
        fclose($config);

        $output->writeln('<header>OK</header>');


        if(!is_dir($this->appDir)) {
            $output->writeln('<header>Creating application direcotry...</header>');
            $this->recurseCopy('appTemplate/mastiffApp', '../mastiffApp');
            $output->writeln('<header>OK</header>');
        }
    }
}