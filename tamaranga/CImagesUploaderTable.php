<?php

namespace mastiff\tamaranga;
use bff;
use func;


class CImagesUploaderTable extends \CImagesUploaderTable
{
    protected $directory = null;

    // Standard field in images tables
    protected $orderByField = 'num';

    public function __construct()
    {
        parent::init();
        $this->initSettings();
        $this->userID = \User::id();
    }

    /**
     * @return null
     */
    public function getOrderByField()
    {
        return $this->orderByField;
    }

    /**
     * @param null $orderByField
     */
    public function setOrderByField($orderByField)
    {
        $this->orderByField = $orderByField;
        return $this;
    }

    /**
     * @param array $aSizes
     * @return $this
     */
    public function setSizesArray(array $aSizes)
    {
        $this->sizes = $aSizes;
        return $this;
    }

    /**
     * @param $sPath
     * @return $this
     */
    public function setPathDir($sPath)
    {
        $this->directory = $sPath;
        return $this;
    }

    /**
     * @param $sKey
     * @return $this
     */
    public function setForeignKey($sKey)
    {
        $this->fRecordID = $sKey;
        return $this;
    }

    /**
     * @param $sTable
     * @return $this
     */
    public function setTable($sTable)
    {
        $this->tableRecords = $sTable;
        return $this;
    }

    /**
     * @param $sTable
     * @return $this
     */
    public function setTableImages($sTable)
    {
        $this->tableImages = $sTable;
        return $this;
    }

    /**
     * @param $nImagesLimit
     * @return $this
     */
    public function setImagesLimit($nImagesLimit)
    {
        $this->limit = $nImagesLimit;
        return $this;
    }

    /**
     * @param $bUseCache
     * @return $this
     */
    public function isCache($bUseCache)
    {
        $this->useFav = $bUseCache;
        if($bUseCache) {
            foreach ($this->sizes as $k => $v) {
                # ключ размера => поле в базе
                if(isset($v['cache']) && !empty($v['cache'])) {
                    $this->sizesFav[$k] = 'img_' . $k;
                }
            }
        }
        return $this;
    }

    /**
     * @param $nRecordID
     * @return $this
     */
    public function setRecordID($nRecordID)
    {
        $this->recordID = $nRecordID;
        return $this;
    }

    /**
     *
     */
    public function initSettings()
    {
        $this->path = bff::path($this->directory, 'images');
        $this->pathTmp = bff::path('tmp', 'images');
        $this->url = bff::url($this->directory, 'images');
        $this->urlTmp = bff::url('tmp', 'images');

        $this->folderByID = true; # раскладываем файлы изображений по папкам (id(5)=>0, id(1005)=>1, ...)
        $this->filenameLetters = 8; # кол-во символов в названии файла
        $this->maxSize = 5242880; # 2мб (2мб: 2097152, 5мб: 5242880)

        $this->minWidth = 20;
        $this->minHeight = 20;
        $this->maxWidth = 5000;
        $this->maxHeight = 5000;
    }

    /**
     * Получаем минимальную ширину изображения
     * @return mixed
     */
    public function getMinWidth() {
        return $this->minWidth;
    }

    /**
     * Задаем минимальную ширину изображения
     * @param $value
     */
    public function setMinWidth($value) {
        $this->minWidth = $value;
        return $this;
    }

    /**
     * Получаем минимальную высоту изображения
     * @return mixed
     */
    public function getMinHeight() {
        return $this->minHeight;
    }

    /**
     * Задаем минимальную высоту изображения
     * @param $value
     */
    public function setMinHeight($value) {
        $this->minHeight = $value;
        return $this;
    }

    /**
     * Получаем максимальную ширину изображения
     * @return mixed
     */
    public function getMaxWidth() {
        return $this->maxWidth;
    }

    /**
     * Задаем максимальную ширину изображения
     * @param $value
     */
    public function setMaxWidth($value) {
        $this->maxWidth = $value;
        return $this;
    }

    /**
     * Получаем максимальную высоту изображения
     * @return mixed
     */
    public function getMaxHeight() {
        return $this->maxHeight;
    }

    /**
     * Задаем максимальную высоту изображения
     * @param $value
     */
    public function setMaxHeight($value) {
        $this->maxHeight = $value;
        return $this;
    }

    /**
     * @param $sSizePrefix
     * @return string
     */
    public function urlDefault($sSizePrefix)
    {
        return $this->url . 'def-' . $sSizePrefix . '.png';
    }

    /**
     * Получаем данные об изображениях указанных брендов
     * @param array $aBrandsID ID брендов
     * @return array массив параметров изображений сформированных: array(brandID=>data, ...)
     */
    public function getItemsImagesData($aBrandsID)
    {
        if (empty($aItemsID)) {
            return array();
        }
        if (!is_array($aBrandsID)) {
            $aBrandsID = array($aBrandsID);
        }

        $aData = $this->db->select('SELECT * FROM ' . $this->tableImages . '
                    WHERE ' . $this->fRecordID . ' IN (' . join(',', $aBrandsID) . ')
                    ORDER BY ' . $this->getOrderByField()
        );
        if (!empty($aData)) {
            $aData = func::array_transparent($aData, $this->fRecordID, false);
        }

        return $aData;
    }

    /**
     * Проверяем наличие данных о загруженном изображении по ID изображения
     * @param integer $nImageID ID изображения
     * @return boolean true - есть данные
     */
    public function imageDataExists($nImageID)
    {
        if (empty($nImageID)) {
            return false;
        }

        $res = $this->db->one_data('SELECT id FROM ' . $this->tableImages . '
                    WHERE id = :imageID AND ' . $this->fRecordID . ' = :recordID
                    LIMIT 1', array(':imageID' => $nImageID, ':recordID' => $this->recordID)
        );
        return ! empty($res);
    }

    /**
     * Получаем дату самого последнего добавленного изображения
     * @param boolean $buildHash сформировать hash на основе даты
     * @return integer|string
     */
    public function getLastUploaded($buildHash = true)
    {
        $lastUploaded = $this->db->one_data('SELECT MAX(created) FROM ' . $this->tableImages . '
                    WHERE ' . $this->fRecordID . ' = :id
                    LIMIT 1', array(':id' => $this->recordID)
        );
        if (!empty($lastUploaded)) {
            $lastUploaded = strtotime($lastUploaded);
        } else {
            $lastUploaded = mktime(0, 0, 0, 1, 1, 2000);
        }

        return ($buildHash ? $this->getLastUploadedHash($lastUploaded) : $lastUploaded);
    }

    /**
     * Формируем hash на основе даты самого последнего добавленного изображения
     * @return integer
     */
    public function getLastUploadedHash($lastUploaded)
    {
        $base64 = base64_encode($lastUploaded);

        return md5(strval($lastUploaded - 1000) . SITEHOST . $base64) . '.' . $base64;
    }

    /**
     * Выполняем проверку, загружались ли новые изображения
     * @param string $lastUploaded hash даты последнего загруженного изображения
     * @return boolean
     */
    public function newImagesUploaded($lastUploaded)
    {
        # проверка hash'a
        if (empty($lastUploaded) || ($dot = strpos($lastUploaded, '.')) !== 32) {
            return true;
        }
        $date = intval(base64_decode(mb_substr($lastUploaded, $dot + 1)));
        if ($this->getLastUploadedHash($date) !== $lastUploaded) {
            return true;
        }
        # выполнялась ли загрузка новых изображений
        if ($this->getLastUploaded(false) > intval($date)) {
            return true;
        }

        return false;
    }

    /**
     * Возвращает ключ максимального размера изображений
     * @return string
     */
    public function getMaxSizeKey()
    {
        $sizes = $this->getSizes();
        return key(end($sizes));
    }

    /**
     * Обработка изображений
     * @return null
     */
    public function imgProcess()
    {
        $aResponse = array();
        $sAction = $this->input->getpost('act');

        switch ($sAction) {
            case 'upload': # загрузка изображений
            {
                $mResult = $this->uploadQQ();
                $aResponse = [
                    'success' => ($mResult !== false && $this->errors->no())
                ];

                if ($mResult !== false) {
                    $aResponse = array_merge($aResponse, $mResult);
                    $aResponse = array_merge($aResponse, $this->getURL($mResult, array_keys($this->sizes), empty($this->recordID))
                    );
                }
                $aResponse['errors'] = $this->errors->get();
                $this->ajaxResponse($aResponse, true, false, true);
            }
                break;
            case 'saveorder': # сохранение порядка изображений
            {
                $img = $this->input->post('img', TYPE_ARRAY);
                if (!$this->saveOrder($img, false, true)) {
                    $this->errors->impossible();
                }
            }
                break;
            case 'delete': # удаление изображений
            {
                $nImageID = $this->input->post('image_id', TYPE_UINT);
                $sFilename = $this->input->post('filename', TYPE_STR);
                if (!$nImageID && empty($sFilename)) {
                    $this->errors->impossible();
                    break;
                }
                if ($nImageID) {
                    $this->deleteImage($nImageID);
                } else {
                    $this->deleteTmpFile($sFilename);
                }
            }
                break;
            case 'delete-all': # удаление всех изображений
            {
                if ($this->recordID) {
                    $this->deleteAllImages(true);
                } else {
                    $sFilename = $this->input->post('filenames', TYPE_ARRAY_STR);
                    $this->deleteTmpFile($sFilename);
                }
            }
                break;
            default:
            {
                $this->errors->impossible();
            }
                break;
        }

        $this->ajaxResponseForm($aResponse);
        return null;
    }
}