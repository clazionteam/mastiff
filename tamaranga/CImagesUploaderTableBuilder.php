<?php

namespace mastiff\tamaranga;


class CImagesUploaderTableBuilder
{
    protected $fRecordID;
    protected $imagesLimit;
    protected $sizes;
    protected $directory;
    protected $tableRecords;
    protected $tableImages;
    protected $isCache;
    protected $recordID;

    /**
     * @param array $aSizes
     * @return $this
     */
    public function setSizesArray(array $aSizes)
    {
        $this->sizes = $aSizes;
        return $this;
    }

    /**
     * @param $sPath
     * @return $this
     */
    public function setPathDir($sPath)
    {
        $this->directory = $sPath;
        return $this;
    }

    /**
     * @param $sKey
     * @return $this
     */
    public function setForeignKey($sKey)
    {
        $this->fRecordID = $sKey;
        return $this;
    }

    /**
     * @param $sTable
     * @return $this
     */
    public function setTable($sTable)
    {
        $this->tableRecords = $sTable;
        return $this;
    }

    /**
     * @param $sTable
     * @return $this
     */
    public function setTableImages($sTable)
    {
        $this->tableImages = $sTable;
        return $this;
    }

    /**
     * @param $nImagesLimit
     * @return $this
     */
    public function setImagesLimit($nImagesLimit)
    {
        $this->imagesLimit = $nImagesLimit;
        return $this;
    }

    /**
     * @param $bUseCache
     * @return $this
     */
    public function isCache($bUseCache)
    {
        $this->useFav = $bUseCache;
        return $this;
    }

    /**
     * @param $nRecordID
     */
    public function setRecordID($nRecordID)
    {
        $this->recordID = $nRecordID;
    }

    /**
     * @return CImagesUploaderTable
     */
    public function getInstance()
    {
        $oImageUploader = new CImagesUploaderTable();
        $oImageUploader
            ->setForeignKey($this->fRecordID)
            ->setImagesLimit($this->imagesLimit)
            ->setPathDir($this->directory)
            ->setTable($this->tableRecords)
            ->setTableImages($this->tableImages)
            ->setSizesArray($this->sizes)
            ->isCache($this->isCache)
            ->setRecordID($this->recordID)
            ->initSettings();
        return $oImageUploader;
    }
}