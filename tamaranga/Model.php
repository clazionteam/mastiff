<?php

namespace mastiff\tamaranga;


class Model extends \Model
{
    /**
     * Подготовка полей таблицы для построения запроса
     * @param array $aFields поля
     * @param string $sTablePrefix префикс таблицы
     * @return array
     */
    protected function prepareFields(array $aFields, $sTablePrefix)
    {
        $res = array();
        foreach($aFields as &$field) {
            $res[] = $sTablePrefix . '.' . $field;
        }
        return $res;
    }

    /**
     * Обьединение полей в строку
     * @param array $aFields поля
     * @return string
     */
    protected function joinFields(array $aFields)
    {
        return join(',', $aFields);
    }

    /**
     * Подготовка полей для построения запроса
     * @param array $aFields поля
     * @param null|string $sTablePrefix префикс таблицы
     * @return string
     */
    protected function prepareQueryFields(array $aFields, $sTablePrefix = null)
    {
        if(!is_null($sTablePrefix)) {
            $aPreparedFields = $this->prepareFields($aFields, $sTablePrefix);
            return $this->joinFields($aPreparedFields);
        } else {
            return $this->joinFields($aFields);
        }
    }

    /**
     * Подготовка пролей для построения запроса с агрегационной функцией
     * @param array $aFields поля
     * @param string $sFunction функция (COUNT/AVG/MAX/MIN ... )
     * @param null|string $sTablePrefix
     * @return string
     */
    protected function prepareQueryAggregateFields($aFields, $sFunction, $sTablePrefix = null)
    {
        foreach($aFields as &$field) {
            $field = $sFunction
                . '('
                . (!is_null($sTablePrefix) ?$sTablePrefix . '.' : '')
                . $field
                . ') as '
                . $sFunction . '_' . $field;
        }
        return $this->joinFields($aFields);
    }

    /**
     * Подготовка limit offset
     * @param int $nLimit
     * @param int $nOffset
     * @return string
     */
    protected function prepareLimitOffset($nLimit = null, $nOffset = null)
    {
        return !is_null($nLimit) ? ' LIMIT ' . (!is_null($nOffset) ? $nOffset . ', ' : '' ) . $nLimit : '';
    }

    /**
     * Получение данных из таблицы переводов
     * @param string $sTable таблица с данными
     * @param string $sLangTable таблица с переводами
     * @param array $aForeignKey внешний ключ в виде ['field_in_table' => 'field_in_reference_table']
     * @param int $nItemID id элемента, для которого получаем перевод
     * @param string $sLangKey поле с языком
     * @return array
     */
    protected function langDataAll($sTable, $sLangTable, array $aFields, array $aForeignKey, $nItemID, $sLangKey)
    {
        $sTableID = key($aForeignKey);
        $sLangTableID = $aForeignKey[$sTableID];
        $aFields[] = $sLangKey;
        $sFields = $this->prepareQueryFields($aFields, 'L');
        $result = $this->db->select(
            'SELECT ' . $sFields . '
              FROM ' .$sLangTable . ' L 
              INNER JOIN ' . $sTable . ' T ON T.' . $sTableID .' = L.' . $sLangTableID.'
              WHERE L.' . $sLangTableID . ' = :id', ['id' => $nItemID]
        );

        $langRes = [];
        foreach ($result as &$res) {
            $lng = $res[$sLangKey];
            unset($res[$sLangKey]);

            foreach ($res as $r => $val) {
                $langRes[$r][$lng] = $val;
            }
        }
        return $langRes;
    }

    /**
     * Выделение подмиссива из массива по ключам
     * @param $aData
     * @param $aFields
     * @return array
     */
    protected function extractFields($aData, $aFields) {
        $aRes = [];
        foreach ($aData as $key => $data) {
            if(in_array($key, $aFields) && !empty($data) && is_string($key)) {
                $aRes[$key] = $data;
            }
        }
        return $aRes;
    }

    /**
     * Сохранение данных в таблицу переводов
     * @param string $sLangTable таблица с переводами
     * @param array $aForeignKey внешний ключ в виде ['field_in_table' => 'field_in_reference_table']
     * @param int $nItemID id элемента, для которого получаем перевод
     * @param string $sLangKey поле с языком
     */
    public function langDataAllSave($aData, $sLangTable, array $aForeignKey, $nItemID, $sLangKey)
    {
        $aTransformedData = [];
        $sFK = $aForeignKey[key($aForeignKey)];
        foreach ($aData as $data => $value) {
            if (is_string($value)) {
                $aValue[LNG] = $value;
                $value = $aValue;
                unset($aValue);
            }
            if(!is_array($value)) continue;
            foreach ($value as $lang => $val) {
                $aTransformedData[$lang][$data] = $val;
                $aTransformedData[$lang][$sLangKey] = $lang;
                $aTransformedData[$lang][$sFK] = $nItemID;
            }
        }

        $bTransactionState = true;
        try {
            $this->db->begin();
            //TODO Optimize this
            $this->db->delete($sLangTable, [$sFK => $nItemID]);

            foreach ($aTransformedData as $data) {
                $this->insert($sLangTable, $data);
            }
            $this->db->commit();
        } catch (\Exception $e) {
            $bTransactionState = $this->setTransactionRollback($e);
        }
        return $bTransactionState;
    }

    /**
     * Вызывает PDO rollback, пишет в log сообщение о падении транзакции
     * @param $e - объект Exception
     * @return bool - false для установки флага статуса транзакии
     */
    public function setTransactionRollback($e)
    {
        $this->db->rollback();
        \bff::log("Transaction filed: \n " . $e->getMessage());
        return false;
    }

    /**
     * Получение данных из таблицы переводов в суффиксном формате (title_en, title_ru ...)
     * @param string $sTable таблица с данными
     * @param string $sLangTable таблица с переводами
     * @param array $aForeignKey внешний ключ в виде ['field_in_table' => 'field_in_reference_table']
     * @param int $nItemID id элемента, для которого получаем перевод
     * @param array $aFields поля, которые надо получить
     * @return array
     */
    protected function langDataAllSuffix($sTable, $sLangTable, array $aForeignKey, $nItemID, array $aFields)
    {
        $sTableID = key($aForeignKey);
        $sLangTableID = $aForeignKey[$sTableID];
        $result = $this->db->select(
            'SELECT * 
              FROM ' .$sLangTable . ' L 
              INNER JOIN ' . $sTable . ' T ON T.' . $sTableID .' = L.' . $sLangTableID.'
              WHERE L.' . $sLangTableID . ' = :id', ['id' => $nItemID]
        );

        $langRes = [];
        foreach ($result as &$res) {
            foreach ($res as $field => $val) {
                $sFieldPure = substr($field, 0, strlen($field) - 4);
                $sLangPure = substr($field, strlen($field) - 3, strlen($field) - 1);
                if(in_array($sFieldPure, $aFields)) {
                    $langRes[$sFieldPure][$sLangPure] = $val;
                }
            }
        }
        return $langRes;
    }

    /**
     * Получение данных из таблицы переводов в суффиксном формате (title_en, title_ru ...) для конкретного языка
     * @param string $sTable таблица с данными
     * @param string $sLangTable таблица с переводами
     * @param array $aForeignKey внешний ключ в виде ['field_in_table' => 'field_in_reference_table']
     * @param int $nItemID id элемента, для которого получаем перевод
     * @param array $aFields поля, которые надо получить
     * @param string $sLang код языка
     * @return mixed
     */
    protected function langDataSuffix($sTable, $sLangTable, array $aForeignKey, $nItemID, array $aFields, $sLang = LNG)
    {
        $aData = $this->langDataAllSuffix($sTable, $sLangTable, $aForeignKey, $nItemID, $aFields);
        return $aData[$sLang];
    }

    /**
     * Получение данных из таблицы переводов
     * @param string $sTable таблица с данными
     * @param string $sLangTable таблица с переводами
     * @param array $aForeignKey внешний ключ в виде ['field_in_table' => 'field_in_reference_table']
     * @param int $nItemID id элемента, для которого получаем перевод
     * @param string $sLangKey поле с языком
     * @param string $sLang код языка
     * @return mixed
     */
    protected function langData($sTable, $sLangTable, array $aFields, array $aForeignKey, $nItemID, $sLangKey, $sLang = LNG)
    {
        $sTableID = key($aForeignKey);
        $sLangTableID = $aForeignKey[$sTableID];
        $aFields[] = $sLangKey;
        $sFields = $this->prepareQueryFields($aFields, 'L');
        return $this->db->select(
            'SELECT '. $sFields .' 
              FROM ' .$sLangTable . ' L 
              INNER JOIN ' . $sTable . ' T ON T.' . $sTableID .' = L.' . $sLangTableID.'
              WHERE L.' . $sLangTableID . ' = :id AND L.'.$sLangKey.'= :lang', ['id' => $nItemID, 'lang' => $sLang]
        );
    }

    /**
     * Возвращает пустой массив переводов или в формате поумолчению
     * @param bool $bValueEmpty - (false - формате поумолчению)
     * @param array $aFields - список мультиязычных полей
     * @return array
     */
    public function getLanguages($bValueEmpty = true, array $aFields = [])
    {
        $aLanguages = $this->locale->getLanguages(true);
        if ( ! $bValueEmpty) {
            return $aLanguages;
        }
        if (is_array($aLanguages)) {
            $aLangsData = [];
            foreach ($aLanguages as $lang) {
                $aLangsData[$lang] = '';
            }
            if (empty($aFields) || ! is_array($aFields)) {
                return $aLangsData;
            }
            $aFieldsData = [];
            foreach ($aFields as $v) {
                $aFieldsData[$v] = $aLangsData;
            }
            return $aFieldsData;
        }
    }

    /**
     * Выполняем INSERT запрос
     * @param string $table название таблицы
     * @param array $fields массив параметров для вставки
     * @param string|array|boolean $returnID возвращать ID вновь добавленной записи (pgsql: несколько данных)
     * @param array $bind доп. параметры запроса для bind'a
     * @return integer ID добавленной записи
     */
    public function insert($table, array $fields = [], $returnID = 'id', $bind = [])
    {
        foreach ($fields as &$f) {
            $f = $this->db->str2sql($f);
        }
        $query = 'INSERT INTO ' . $table . ' (`' . join('`,`', array_keys($fields)) . '`) VALUES (' . join(',', array_values($fields)) . ')';
        if ($returnID !== false) {
                $res = $this->db->exec($query, $bind);
                if($res) {
                    return $this->db->insert_id($table, $returnID);
                }
        }
        return false;
    }
}