<?php

namespace mastiff\tamaranga;


class Module extends \Module
{
    /**
     * @inheritdoc
     */
    public function configSave($aConfig, $bIncludeDynamic = false)
    {
        if ( ! empty($aConfig) && is_array($aConfig)) {
            foreach ($aConfig as $k => $v) {
                if (is_array($v)) {
                    $aConfig[$k] = serialize($v);
                }
            }
        }
        return parent::configSave($aConfig, $bIncludeDynamic);
    }

    /**
     * @inheritdoc
     */
    public function configLoad(array $defaults = array(), $mPrefix = false)
    {
        $aConfig = parent::configLoad($defaults, $mPrefix);
        if ( ! empty($aConfig) && is_array($aConfig)) {
            foreach ($aConfig as $k => $v) {
                if ($this->isSerialized($v)) {
                    $aConfig[$k] = \func::unserialize($v);
                }
            }
        }
        return $aConfig;
    }

    /**
     * Tests if an input is valid PHP serialized string.
     *
     * Checks if a string is serialized using quick string manipulation
     * to throw out obviously incorrect strings. Unserialize is then run
     * on the string to perform the final verification.
     *
     * Valid serialized forms are the following:
     * <ul>
     * <li>boolean: <code>b:1;</code></li>
     * <li>integer: <code>i:1;</code></li>
     * <li>double: <code>d:0.2;</code></li>
     * <li>string: <code>s:4:"test";</code></li>
     * <li>array: <code>a:3:{i:0;i:1;i:1;i:2;i:2;i:3;}</code></li>
     * <li>object: <code>O:8:"stdClass":0:{}</code></li>
     * <li>null: <code>N;</code></li>
     * </ul>
     *
     * @author		Chris Smith <code+php@chris.cs278.org>
     * @copyright	Copyright (c) 2009 Chris Smith (http://www.cs278.org/)
     * @license		http://sam.zoy.org/wtfpl/ WTFPL
     * @param		string	$sValue	Value to test for serialized form
     * @param		mixed	$result	Result of unserialize() of the $sValue
     * @return		boolean	True if $sValue is serialized data, otherwise false
     */
    public function isSerialized($sValue, &$result = null)
    {
        if ( ! is_string($sValue)) {
            return false;
        }
        if ($sValue === 'b:0;') {
            $result = false;
            return true;
        }
        $sLength = strlen($sValue);
        $sEnd = '';
        if (!isset($sValue[0])) {
            return false;
        }
        switch ($sValue[0]) {
            case 's':
                if ($sValue[$sLength - 2] !== '"') {
                    return false;
                }
            case 'b':
            case 'i':
            case 'd':
                $sEnd .= ';';
            case 'a':
            case 'O':
                $sEnd .= '}';
                if ($sValue[1] !== ':') {
                    return false;
                }
                switch ($sValue[2]) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                        break;
                    default:
                        return false;
                }
            case 'N':
                $sEnd .= ';';
                if ($sValue[$sLength - 1] !== $sEnd[0]) {
                    return false;
                }
                break;
            default:
                return false;
        }
        if (($result = unserialize($sValue)) === false) {
            $result = null;
            return false;
        }
        return true;
    }
}