<?php

namespace mastiff\tamaranga;


class Tpl
{
    protected static $dir = 'mastiff';
    /**
     * Подключение myowncode js
     * @param $aJs
     */
    protected static function includeJSPath($aJs, $path)
    {
        if(!is_array($aJs)) {
            $aJs = [$aJs];
        }
        foreach ($aJs as &$file) {
            if (preg_match('/^(|http:\/\/|https:\/\/)(.+)(\/)(.+)(.js)$/i', $file)) {
                continue;
            } elseif (preg_match('/^(\w+)(\/|\\\)(\w+)$/i', $file)) {
                $file = self::$dir.DS.$file;
            } else {
                $nIsDebug = strpos($file, '-debug');
                if($nIsDebug > 0) {
                    $dir = substr($file, 0, strlen($file) - strlen('-debug'));
                } else {
                    $dir = $file;
                }
                $file = $path . DS . $dir . DS. $file;
            }
        }
        \tpl::includeJS($aJs, false);
    }

    /**
     * Подключение myowncode css
     * @param $aCss
     */
    protected static function includeCSSPath($aCss, $path)
    {
        if(!is_array($aCss)) {
            $aCss = [$aCss];
        }
        foreach ($aCss as &$file) {
            if (preg_match('/^(|http:\/\/|https:\/\/)(.+)(\/)(.+)(.css)$/i', $file)) {
                continue;
            } elseif (preg_match('/^(\w+)(\/|\\\)(\w+)$/i', $file)) {
                $file = self::$dir.DS.$file;
            } else {
                $file = $path . DS . $file . DS. $file;
            }
            $file = DS . 'css' . DS . $file;
        }
        \tpl::includeCSS($aCss, false);
    }

    public static function includeJS($aJs)
    {
        self::includeJsPath($aJs, self::$dir);
    }

    /**
     * Подключение myowncode css
     * @param $aCss
     */
    public static function includeCSS($aCss)
    {
        self::includeCSSPath($aCss, self::$dir);
    }
}